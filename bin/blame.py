#!/usr/bin/env python3

from git import *
import argparse
from pygments import highlight
from pygments.lexers import guess_lexer_for_filename, guess_lexer, TextLexer
from pygments.formatters import TerminalFormatter
from pygments.util import ClassNotFound as PygmentsError
from colorama import init, Fore, Style
import sys
import pydoc
import os.path

def main():
    init(strip=False)

    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()

    repo = Repo(".")

    assert not repo.bare

    if os.path.isabs(args.filename):
        path = args.filename
    else:
        path = os.path.join(os.environ['GIT_PREFIX'], args.filename)

    blame = repo.blame('HEAD', path)

    filecontent = []

    cols = [Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.CYAN]
    authors = {}
    maxauthlen = 0

    for commit, lines in blame:
        auth = commit.author.name
        if auth not in authors:
            authors[auth] = cols[len(authors) % len(cols)]
            if maxauthlen < len(auth):
                maxauthlen = len(auth)
        filecontent += lines

    filecontent = "\n".join(filecontent)
    try:
        lex = guess_lexer_for_filename(args.filename, filecontent)
    except PygmentsError as e:
        try:
            lex = guess_lexer(filecontent)
        except PygmentsError:
            lex = TextLexer()

    filehl = highlight(filecontent, lex, TerminalFormatter(bg="dark")).split("\n")

    authorformat = "%%%is" % maxauthlen
    output = []

    try:
        idx = 0
        for commit, lines in blame:
            firstc = Style.BRIGHT
            for line in lines:
                auth = commit.author.name
                output.append(" %s%s%s%s %s %s%s %5i | %s" % (firstc,
                    authors[auth],
                    authorformat % auth,
                    Fore.WHITE,
                    commit.committed_datetime.strftime("%H:%M:%S %a %d %b %Y"),
                    "".join("%02x" % i for i in commit.binsha[0:4]),
                    Style.RESET_ALL,
                    idx+1,
                    filehl[idx]))
                firstc = Style.DIM
                idx += 1

        pydoc.pipepager("\n".join(output), cmd='less -R')
    except (IOError, BrokenPipeError) as e:
        sys.stderr.close()

if __name__ == "__main__":
    main()
