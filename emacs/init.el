;; ===============
;; Package Loading
;; ===============

(require 'package)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("melpa" . "http://melpa.org/packages/")))
(package-initialize)

;; Install use-package if it's not already installed
;; This will be used to manage the rest of the packages.
;; https://github.com/jwiegley/use-package
(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))
;; Automatically install missing packages by default
(require 'use-package-ensure)
(setq use-package-always-ensure t)

(add-to-list 'load-path (concat user-emacs-directory
                                (convert-standard-filename "lisp/")))

;; ==========
;; Appearance
;; ==========

;; Colour Scheme
(use-package base16-theme
  :config
  (load-theme 'base16-gruvbox-light-hard t))

;; =============================
;; Evil Mode & Editing Utilities
;; =============================

;; Make undo/redo more evil
(use-package undo-tree)

;; Core package
(use-package evil
  :after undo-tree
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-<SPACE>") 'company-complete)
  )

;; Keybindings for other mode
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

;; Mode Line
(use-package spaceline
  :after evil
  :init
  (require 'spaceline-config)
  (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
  :config
  (spaceline-spacemacs-theme))

;; ================
;; Language Support
;; ================

;; CMake - in a typical CMake installation on Arch, the support file will be available in Path
(use-package cmake-mode :ensure nil)

;; JSX
(use-package rjsx-mode
  :mode "\\.jsx?$")

;; TS/TSX
(use-package typescript-mode
  :mode (("\\.ts\\'" . typescript-mode)
         ("\\.tsx\\'" . typescript-mode)))

(defun setup-tide-mode ()
  (interactive)
  (defun tide-imenu-index () nil)
  (tide-setup)
  (tide-hl-identifier-mode +1))

(use-package tide
  :config
  (progn
    (add-hook 'typescript-mode-hook #'setup-tide-mode)
    (add-hook 'js-mode-hook #'setup-tide-mode)
    (add-hook 'js2-mode-hook #'setup-tide-mode)
    (add-hook 'rjsx-mode-hook #'setup-tide-mode)))

;; yaml
(use-package yaml-mode)

;; Markdown
(use-package markdown-mode
  :ensure t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;; ======================
;; Generic Code Utilities
;; ======================

;; Indentation detection and set default to 4 spaces
(use-package dtrt-indent
  :init
  (setq tab-width 4)
  (setq-default indent-tabs-mode nil)
  :config
  (define-globalized-minor-mode global-dtrt-indent dtrt-indent-mode
    (lambda () (dtrt-indent-mode 1)))
  (global-dtrt-indent 1))

;; Load files with filename:lineno syntax
(require 'emacs-fetch)

;; Syntax check all the things
(use-package flycheck
  :init
  (global-flycheck-mode)
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

;; General completions
(use-package company
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-minimum-prefix-length 1)
  (global-set-key (kbd "C-SPC") 'company-complete))

;; C++/C/Python etc completion
(use-package ycmd
  :after company
  :init
  (add-hook 'after-init-hook #'global-ycmd-mode)
  (setq ycmd-server-command
        (list "python" "-u" (file-truename "~/.vim/plugged/YouCompleteMe/third_party/ycmd/ycmd")))
  (setq ycmd-global-config (file-truename "~/.vim/ycm_extra_conf.py"))
  (setq ycmd-startup-timeout 5))

;; Integration with company-mdoe
(use-package company-ycmd
  :after ycmd
  :init
  (company-ycmd-setup))

;; Integration with flycheck
(use-package flycheck-ycmd
  :after ycmd
  :init
  (flycheck-ycmd-setup))

(use-package realgud)

;; ==============
;; Fix Annoyances
;; ==============

;; No startup screen
(setq inhibit-startup-screen t)
;; Maximise windows on startup
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;; Don't mess with my config >:( (not actually loading this file)
(setq custom-file (expand-file-name "ignore-custom.el" user-emacs-directory))
;; Hide the toolbar in the GUI
(tool-bar-mode -1)
;; No Tildes
(setq make-backup-files nil)
;; Autosave is alright though
;;(setq auto-save-file-name-transforms
;;  `((".*" (locate-user-emacs-file "auto-save") t)))
;; Line numbers
(global-display-line-numbers-mode)
;; UTF-8 all the things?
(prefer-coding-system 'utf-8)
;; Workaround UTF-8 encoding bugs (shows up as a failure to encode UTF-8 in ycmd and gdb)
;; See https://github.com/abingham/emacs-ycmd/issues/496
(setq request-backend 'url-retrieve)
;; Better GDB GUI
(setq gdb-many-windows t)
;; Better frame titles
(setq-default frame-title-format '("%b (%f) [%m]"))

(setq cua-enable-cua-keys nil)
(cua-mode t)
;; Normal cut/copy/paste shortcuts
(define-key evil-insert-state-map (kbd "C-c") 'cua-copy-region)
(define-key evil-insert-state-map (kbd "C-v") 'cua-paste)
(define-key evil-insert-state-map (kbd "C-x") 'cua-cut-region)
(define-key evil-insert-state-map (kbd "C-z") 'undo-tree-undo)
(define-key evil-insert-state-map (kbd "C-y") 'undo-tree-redo)
(define-key evil-visual-state-map (kbd "C-x") 'cua-cut-region)
(define-key evil-visual-state-map (kbd "C-c") 'cua-copy-region)
(define-key evil-visual-state-map (kbd "C-v") 'cua-paste)

;; Slow down scrolling a bit
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))

(defun my-search-symbol-at-click (@click)
  (interactive "e")
  (let ((p1 (posn-point (event-start @click))))
    (goto-char p1)
    (evil-search-word-forward)))
(defun my-search-symbol-at-click-reverse (@click)
  (interactive "e")
  (let ((p1 (posn-point (event-start @click))))
    (goto-char p1)
    (evil-search-word-backward)))

(bind-key "<S-mouse-1>" 'my-search-symbol-at-click)
(bind-key "<S-down-mouse-1>" 'my-search-symbol-at-click)
(bind-key "<S-mouse-3>" 'my-search-symbol-at-click-reverse)
(bind-key "<S-down-mouse-3>" 'my-search-symbol-at-click-reverse)
