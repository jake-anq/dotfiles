vim.cmd [[packadd packer.nvim]]

-- Disable netrw for nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

---- Packages ----

require('packer').startup(function()
    -- System & Misc
    use 'wbthomason/packer.nvim'

    use 'airblade/vim-gitgutter'
    use 'tpope/vim-abolish'

    use 'kopischke/vim-fetch'

    use 'nvim-tree/nvim-tree.lua'

    -- Languages
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }

    use 'chrisbra/csv.vim'
    use 'habamax/vim-godot'

    use 'neovim/nvim-lspconfig'
    use 'hrsh7th/nvim-cmp'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-path'
    use 'L3MON4D3/LuaSnip'
    use 'saadparwaiz1/cmp_luasnip'

    use 'tpope/vim-sleuth'

    -- Visual
    use 'EdenEast/nightfox.nvim'
    use {
        'nvim-lualine/lualine.nvim',
        requires = { 'nvim-tree/nvim-web-devicons', opt = true }
    }

    use {
      "folke/trouble.nvim",
      requires = "nvim-tree/nvim-web-devicons",
      config = function()
        require("trouble").setup {
          -- your configuration comes here
          -- or leave it empty to use the default settings
          -- refer to the configuration section below
        }
      end
    }
    use 'nvim-tree/nvim-web-devicons'
end)

---- Visual ----

vim.cmd(":colorscheme duskfox")
require('lualine').setup({
  tabline = {
    lualine_a = {
      {
        'tabs',
        max_length = function()
          return vim.o.columns
        end,
        mode = 2,
      }
    },
  }
})

vim.opt.number = true
vim.opt.foldenable = false
vim.opt.linebreak = true
vim.opt.wildmenu = true
vim.opt.showmatch = true
vim.opt.hlsearch = true
vim.opt.title = true

vim.opt.colorcolumn = {100}

vim.opt.guifont = "Hack Nerd Font Mono:h9"

---- Misc ----
vim.opt.compatible = false
-- Show tabs and trailing whitespace
vim.opt.listchars:append {
  tab = "» ",
  trail = "…",
}
vim.opt.list = true

---- Languages ----

vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4

require'nvim-treesitter.configs'.setup {
  -- One of "all", "maintained" (parsers with maintainers), or a list of languages
  ensure_installed = "all",
  -- Install languages synchronously (only applied to `ensure_installed`)
  sync_install = false,
  -- List of parsers to ignore installing
  ignore_install = {},
  highlight = {
    -- `false` will disable the whole extension
    enable = true,
    -- list of language that will be disabled
    disable = {},
    additional_vim_regex_highlighting = false,
  },
  indent = {
    enable = true,
  },
}

---- Extra file extensions ----
vim.filetype.add({
  extension = {
    wgsl = "wgsl",
  }
})

---- Keybindings ----

function map(mode, shortcut, command)
    vim.api.nvim_set_keymap(mode, shortcut, command, {noremap=true, silent=true})
end
function bufmap(bufnr, mode, shortcut, command)
    vim.api.nvim_buf_set_keymap(bufnr, mode, shortcut, command, {noremap=true, silent=true})
end

map("v", "<C-c>", "\"+yi")
map("v", "<C-x>", "\"+c")
map("v", "<C-v>", "c<ESC>\"+p")
map("i", "<C-v>", "<C-r><C-o>+")

-- LuaSnips keybindings
ls = require'luasnip'
vim.keymap.set({"i", "s"}, "<Tab>", function() ls.jump( 1) end, {silent = true})
vim.keymap.set({"i", "s"}, "<S-Tab>", function() ls.jump(-1) end, {silent = true})
vim.keymap.set({"i", "s"}, "<C-Tab>", function()
	if ls.choice_active() then
		ls.change_choice(1)
	end
end, {silent = true})

-- Language server keybind setup

map('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>')
map('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>')
map('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>')
map('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  bufmap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>')
  bufmap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
  bufmap(bufnr, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>')
  bufmap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>')
  bufmap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>')
  bufmap(bufnr, 'n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>')
  bufmap(bufnr, 'n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>')
  bufmap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>')
  bufmap(bufnr, 'n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>')
  bufmap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>')
  bufmap(bufnr, 'n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>')
  bufmap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>')
  bufmap(bufnr, 'n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')
end

vim.lsp.set_log_level("warn") -- avoiding large logfiles...

-- Add additional capabilities supported by nvim-cmp
local capabilities = require('cmp_nvim_lsp').default_capabilities()

lc = require'lspconfig'
lc.ruff_lsp.setup{}
lc.pylyzer.setup{}
lc.pyright.setup{}
lc.tsserver.setup{}
lc.rust_analyzer.setup {
  settings = {
    ['rust-analyzer'] = {
      assist = {
        importEnforceGranularity = true,
        importPrefix = 'crate',
      },
      cargo = {
        allFeatures = true,
      },
      checkOnSave = {
        command = 'clippy',
      },
      inlayHints = { locationLinks = false },
      diagnostics = {
        enable = true,
        experimental = {
          enable = true,
        },
      },
    },
  },
}

local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      require'luasnip'.lsp_expand(args.body)
    end
  },
  mapping = {
    ['<Up>'] = cmp.mapping.select_prev_item(),
    ['<C-k>'] = cmp.mapping.select_prev_item(),
    ['<Down>'] = cmp.mapping.select_next_item(),
    ['<C-j>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end,
    ['<S-Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end,
  },
  sources = {
    {name = 'nvim_lsp'},
    {name = 'path'},
    {name = 'luasnip'},
  },
}

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline('/', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' }
  }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  })
})

-- Add additional capabilities supported by nvim-cmp
capabilities = require('cmp_nvim_lsp').default_capabilities()

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { 'pyright', 'rls', 'tsserver', 'openscad_lsp', 'svelte' }
local lspconfig = require('lspconfig')
for _, lsp in pairs(servers) do
  lspconfig[lsp].setup {
    capabilities = capabilities,
  }
end
lspconfig['gdscript'].setup {
  capabilities = capabilities,
  cmd = { 'nc', 'localhost', '6005' },
}

-- Project Browser --

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

local function tree_on_attach(bufnr)
  local api = require "nvim-tree.api"

  local function opts(desc)
    return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end

  -- default mappings
  api.config.mappings.default_on_attach(bufnr)

  -- custom mappings
  vim.keymap.set('n', '<2-LeftMouse>', api.node.open.tab,                     opts('Open: New Tab'))
  vim.keymap.set('n', '<CR>',          api.node.open.tab,                     opts('Open: New Tab'))
  vim.keymap.set('n', '<ESC>',         api.tree.close,                        opts('Close'))
end

local HEIGHT_RATIO = 0.8
local WIDTH_RATIO = 0.5

require("nvim-tree").setup({
  modified = {
    enable = true,
  },
  view = {
    float = {
      enable = true,
      -- Centre floating window - based on https://github.com/MarioCarrion/videos/blob/269956e913b76e6bb4ed790e4b5d25255cb1db4f/2023/01/nvim/lua/plugins/nvim-tree.lua
      open_win_config = function()
        local screen_w = vim.opt.columns:get()
        local screen_h = vim.opt.lines:get() - vim.opt.cmdheight:get()
        local window_w = screen_w * WIDTH_RATIO
        local window_h = screen_h * HEIGHT_RATIO
        local window_w_int = math.floor(window_w)
        local window_h_int = math.floor(window_h)
        local center_x = (screen_w - window_w) / 2
        local center_y = ((vim.opt.lines:get() - window_h) / 2)
                         - vim.opt.cmdheight:get()
        return {
          border = "rounded",
          relative = "editor",
          row = center_y,
          col = center_x,
          width = window_w_int,
          height = window_h_int,
        }
      end,
    },
  },
  actions = {
    open_file = {
      quit_on_open = true,
    },
  },
  on_attach = tree_on_attach,
})
local api = require("nvim-tree.api")
vim.keymap.set('n', '`', api.tree.open)

-- Convert all instances of word to uppercase or lowercase
map("n", "<Leader>u", [[:%s/\<<C-r><C-w>\>/\U\O/g<RETURN><C-o>]])
map("n", "<Leader>l", [[:%s/\<<C-r><C-w>\>/\L\O/g<RETURN><C-o>]])

-- Remove trailing whitespace
map("n", "<Leader>w", [[:%s/\s\+$//e<RETURN><C-o>]])

---- Utils

vim.cmd([[
augroup AutoExecutable
   autocmd!
   " Set scripts to be executable from the shell
   autocmd BufWritePost * if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent exe "!chmod +x <afile>" | w | endif | endif
   " If we do this unconditionally, javascript.jsx isn't redetected properly...
   " This will cover most use cases for now
   autocmd BufWritePost * if getline(1) =~ "^#!" | filetype detect | endif
augroup END
]])

vim.lsp.set_log_level("debug")

vim.filetype.add({
  extension = {
    svx = "markdown",
  }
})
