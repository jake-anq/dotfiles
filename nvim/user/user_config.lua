-- use a user/user_config.lua file to provide your own configuration

local M = {}

-- add any null-ls sources you want here
M.setup_sources = function(b)
	return {
		b.code_actions.gitsigns,
	}
end

-- add mason sources to auto-install
M.mason_ensure_installed = {
-- 	null_ls = {
-- 		"stylua",
-- 		"jq",
-- 	},
-- 	dap = {
-- 		"python",
-- 		"delve",
-- 	},
}

-- add servers to be used for auto formatting here
M.formatting_servers = {
	["ruff"] = { "python" },
	["rust_analyzer"] = { "rust" },
	["lua_ls"] = { "lua" },
	["null_ls"] = {
		"javascript",
		"javascriptreact",
		"typescript",
		"typescriptreact",
	},
}

-- options you put here will override or add on to the default options
M.options = {
	opt = {
		confirm = true,
	},
}

-- Set any to false that you want disabled in here.
-- take a look at the autocommands file in lua/core for more information
-- Default value is true if left blank
M.autocommands = {
	alpha_folding = true,
	treesitter_folds = true,
	trailing_whitespace = true,
	remember_file_state = true,
	session_saved_notification = true,
	css_colorizer = true,
	cmp = true,
}

-- set to false to disable plugins
-- Default value is true if left blank
M.enable_plugins = {
	aerial = true,
	alpha = true,
	autotag = true,
	bufferline = true,
	context = true,
	copilot = true,
	dressing = true,
	gitsigns = true,
	hop = true,
	img_clip = true,
	indent_blankline = true,
	lsp_zero = true,
	lualine = true,
	neodev = true,
	neoscroll = true,
	neotree = true,
	session_manager = true,
	noice = true,
	null_ls = true,
	autopairs = true,
	cmp = true,
	colorizer = true,
	dap = true,
	notify = true,
	surround = true,
	treesitter = true,
	ufo = true,
	onedark = false,
	project = true,
	rainbow = true,
	scope = true,
	telescope = true,
	toggleterm = true,
	trouble = true,
	twilight = true,
	whichkey = true,
	windline = true,
	zen = true,
}

-- add extra plugins in here
M.plugins = {
	{
		"nvim-neotest/neotest",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
			"antoinemadec/FixCursorHold.nvim",
		},
	},
	{
		"EdenEast/nightfox.nvim",
	},
	{
		"kopischke/vim-fetch",
    lazy = false,
	}
}

-- add extra configuration options here, like extra autocmds etc.
-- feel free to create your own separate files and require them in here
M.user_conf = function()
	vim.cmd("colorscheme duskfox")

  -- Configure LSPs
  lc = require'lspconfig'
  lc.ruff.setup{}
  lc.svelte.setup{}

  -- Show tabs and trailing whitespace
  vim.opt.listchars:append {
    tab = "» ",
    trail = "…",
  }

  -- Copy/paste
  local map = require("core.utils.utils").map
  map("v", "<C-c>", "\"+yi")
  map("v", "<C-x>", "\"+c")
  map("v", "<C-v>", "c<ESC>\"+p")
  map("i", "<C-v>", "<C-r><C-o>+")

  -- Convert all instances of word to uppercase or lowercase
  map("n", "<Leader>u", [[:%s/\<<C-r><C-w>\>/\U\O/g<RETURN><C-o>]])
  map("n", "<Leader>l", [[:%s/\<<C-r><C-w>\>/\L\O/g<RETURN><C-o>]])

  -- Automatically set executable flag
  vim.cmd([[
  augroup AutoExecutable
     autocmd!
     " Set scripts to be executable from the shell
     autocmd BufWritePost * if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent exe "!chmod +x <afile>" | w | endif | endif
     " Update filetype when saving, since there probably wasn't an extension
     autocmd BufWritePost * if getline(1) =~ "^#!" | filetype detect | endif
  augroup END
  ]])

  -- Svelte markdown support
  vim.filetype.add({
    extension = {
      svx = "markdown",
    }
  })

  -- Don't autowrap badly
  vim.opt.formatoptions:remove{"t"}
  -- Leave my clipboard alone when selecting
  vim.opt.clipboard:remove{"unnamedplus"}
end

return M
