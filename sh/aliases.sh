# vim: set ft=sh :
alias ls='ls --color=auto'
alias ll='ls -lha'
alias la='ls -a'
alias grep='grep --color=auto --exclude-dir=.svn --exclude-dir=.git --exclude-dir=.mypy_cache'
alias make="make -j8"

alias vl='vim -R -'

alias fif="find . -type f | grep -vP '\.git' | xargs -d '\n' grep -I --color=always -n"
alias fid="find . -maxdepth 1 -type f | grep -vP '\.git' | xargs -d '\n' grep -I --color=always -n"

alias pe="perl -MMath::Trig -E 'say eval \$ARGV[0]' -- "

alias hb="haste | xclip -selection c"
alias xc="xclip -selection c"
alias xn="xclip -selection c -rmlastnl"

alias fixswap="sudo sh -c 'swapoff -a && swapon -a'"
alias fixbuffers="sudo sh -c 'echo 1 > /proc/sys/vm/drop_caches'"

function op {
    for file in "$@"
    do
        silent xdg-open "$file"
    done
}

alias em="emacsclient -c -n --alternate-editor="

function pathadd() {
    export PATH="$PATH:$1"
}

function build() {
    if [[ -v DISTCC_HOSTS ]] && silent hash distcc
    then
        PARALLEL_JOBS=$(distcc -j)
    else
        PARALLEL_JOBS=$(nproc)
    fi
    if [ -f Makefile ] || [ -f makefile ]
    then
        if [ -f build.ninja ]
        then
            echo "Error: both makefile and build.ninja present"
        else
            command make -j"$PARALLEL_JOBS" "$@"
        fi
    elif [ -f build.ninja ]
    then
        command ninja -j"$PARALLEL_JOBS" "$@"
    else
        echo "Error: can't find makefile or build.ninja"
    fi
}

alias make=build
alias ninja=build

function unzipf() {
    # Unzip while ensuring that the files will go into a subdirectory
    # Based on https://stackoverflow.com/a/965072/5127165
    if [ $# -ne 1 ] || ! [ -f "$1" ]
    then
        printf '%s\n' "Expected a filename as the first (and only) argument. Aborting."
        return 1
    fi

    extract_dir="."

    # Strip the leading and trailing information about the zip file (leaving
    # only the lines with filenames), then check to make sure *all* filenames
    # contain a /.
    # If any file doesn't contain a / (i.e. is not located in a directory or is
    # a directory itself), exit with a failure code to trigger creating a new
    # directory for the extraction.
    if ! unzip -l "$1" | tail -n +4 | head -n -2 | awk 'BEGIN {lastprefix = ""} {if (match($4, /[^/]+/)) {prefix=substr($4, RSTART, RLENGTH); if (lastprefix != "" && prefix != lastprefix) {exit 1}; lastprefix=prefix}}'
    then
        filename="$(basename -- "$1")"
        extract_dir="${filename%.zip}"
    fi
    
    echo "Extracting $1 to $extract_dir"

    unzip -d "$extract_dir" "$1"
}

function cgdb() {
    INPUTRC="$DOTREPO/cgdb-inputrc" command cgdb "$@"
}
