#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

THEME=molokai

echo "export LS_COLORS='$(vivid generate "$THEME")'" > "$SCRIPT_DIR/ls_colors.sh"
