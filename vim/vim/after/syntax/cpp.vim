syn keyword cType float32_t float64_t

unlet b:current_syntax
syn include @PYTHON syntax/python.vim

unlet b:current_syntax
syn include @C syntax/c.vim

syn region cogSnip matchgroup=Snip start="\[\[\[cog" end="\]\]\]" contains=@PYTHON containedin=@C
syn match cogEnd "\[\[\[end\]\]\]" containedin=@C

hi link Snip SpecialComment
hi link cogEnd SpecialComment
let b:current_syntax = 'cpp'
